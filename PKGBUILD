# Maintainer: tioguda <guda.flavio@gmail.com>

pkgbase=wxgtk-3.0.5
pkgname=(wxgtk2-3.0.5 wxgtk3-3.0.5 wxgtk-common-3.0.5)
pkgver=3.0.5.1
pkgrel=1.9
arch=('x86_64')
url="https://wxwidgets.org"
license=('custom:wxWindows')
makedepends=('gst-plugins-base' 'glu' 'webkit2gtk' 'libnotify' 'gtk2')
options=('!emptydirs')
source=("https://github.com/wxWidgets/wxWidgets/releases/download/v${pkgver}/wxWidgets-${pkgver}.tar.bz2"
        wxgtk-3.05.conf)
sha256sums=('440f6e73cf5afb2cbf9af10cec8da6cdd3d3998d527598a53db87099524ac807'
            'f89cd15e685a9614b1b5d55c202391a8eac81740c2bd1b51e53eb6362e9ffd80')

prepare() {
  mkdir -p wxWidgets-${pkgver}-gtk3

  cd wxWidgets-${pkgver}
  cp -a * ${srcdir}/wxWidgets-${pkgver}-gtk3
}

build() {
  cd wxWidgets-${pkgver}
  export CXXFLAGS="$CXXFLAGS -fabi-version=13"
  ./configure --prefix=/opt/wxgtk-3.0.5 --libdir=/opt/wxgtk-3.0.5/lib --with-gtk=2 --with-opengl --enable-unicode \
    --enable-graphics_ctx --enable-mediactrl --with-regex=builtin \
    --with-libpng=sys --with-libxpm=sys --with-libjpeg=sys --with-libtiff=sys \
    --disable-precomp-headers
  make
  make -C locale allmo

  cd ../wxWidgets-${pkgver}-gtk3
  export CXXFLAGS="$CXXFLAGS -fabi-version=13"
  ./configure --prefix=/opt/wxgtk-3.0.5 --libdir=/opt/wxgtk-3.0.5/lib --with-gtk=3 --with-opengl --enable-unicode \
    --enable-graphics_ctx --enable-mediactrl --enable-webview --with-regex=builtin \
    --with-libpng=sys --with-libxpm=sys --with-libjpeg=sys --with-libtiff=sys \
    --disable-precomp-headers
  make
}

package_wxgtk-common-3.0.5() {
  pkgdesc='Common libraries and headers for wxgtk2 and wxgtk3, v3.0.5 installed in /opt/wxgtk-3.0.5/'
  depends=('zlib' 'gcc-libs' 'expat')

  cd wxWidgets-${pkgver}
  make DESTDIR="${pkgdir}" install
  rm -r "${pkgdir}"/opt/wxgtk-3.0.5/{bin/wx-config,lib/{wx,libwx_gtk*}}

  install -D -m644  ../wxgtk-3.05.conf -t "${pkgdir}/etc/ld.so.conf.d/"
  install -D -m644 docs/licence.txt "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE
}

package_wxgtk2-3.0.5() {
  pkgdesc='GTK+2 implementation of wxWidgets API for GUI, v3.0.5 installed in /opt/wxgtk-3.0.5/'
  depends=('gtk2' 'libgl' 'gst-plugins-base-libs' 'libsm' 'libxxf86vm' 'wxgtk-common-3.0.5' 'libnotify')
  replace=('wxgtk2')
  provides=("wxgtk2=${pkgver}")

  cd wxWidgets-${pkgver}
  make DESTDIR="${pkgdir}" install
  rm -r "${pkgdir}"/opt/wxgtk-3.0.5/{include,share,lib/libwx_base*,bin/wxrc*}

  install -D -m644 docs/licence.txt "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE
}

package_wxgtk3-3.0.5() {
  pkgdesc='GTK+3 implementation of wxWidgets API for GUI, v3.0.5 installed in /opt/wxgtk-3.0.5/'
  depends=('gtk3' 'gst-plugins-base-libs' 'libsm' 'libxxf86vm' 'wxgtk-common-3.0.5' 'libnotify')
  optdepends=('webkit2gtk: for webview support')

  cd wxWidgets-${pkgver}-gtk3
  make DESTDIR="${pkgdir}" install  
  rm -r "${pkgdir}"/opt/wxgtk-3.0.5/{include,share,lib/libwx_base*,bin/wxrc*}
  mv "${pkgdir}"/opt/wxgtk-3.0.5/bin/wx-config{,-gtk3}

  install -D -m644 docs/licence.txt "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE
}
